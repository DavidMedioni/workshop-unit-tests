#!/usr/bin/env python3


def is_at_least_18(age: int) -> bool:
    """
    Return True if age is at least equal to 18, False otherwise.
    """
    return age >= 18


def format_first_name(firstname: str) -> str:
    """Capitalize the string passed as a parameter"""
    return firstname.capitalize()


def format_last_name(lastname) -> str:
    """Return the string passed as a parameter in Uppercase."""
    return lastname.upper()


def _validate_age(age: int):
    if not isinstance(age, int) or not age > 0:
        raise TypeError("Age has to be a positive integer!")
