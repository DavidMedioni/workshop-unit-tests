#!/usr/bin/env python3
import pytest
from user import (
    is_at_least_18,
    format_first_name,
    format_last_name,
    _validate_age,
)


class TestUser:
    @pytest.mark.parametrize(
        "value, expected", [(5, False), (30, True)], ids=["False < 18", "True >= 18"]
    )
    def test_is_at_least_18(self, value, expected):
        assert is_at_least_18(value) == expected

    def test_format_first_name(self):
        assert format_first_name("tartiflette") == "Tartiflette"

    def test_format_last_name(self):
        assert format_last_name("tartiflette") == "TARTIFLETTE"

    @pytest.mark.parametrize(
        "value, expected",
        [(5, None), ("tartiflette", TypeError)],
        ids=["OK int", "KO not int"],
    )
    def test_validate_age(self, value, expected):
        if expected:
            with pytest.raises(expected):
                _validate_age(value)
        else:
            assert _validate_age(value) == expected
