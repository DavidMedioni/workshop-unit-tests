#!/usr/bin/env python3

# Simplest Example


class Rectangle:
    def __init__(lenght, width):
        self.lenght = lenght
        self.width = width


def compute_rectangle_surface(rectangle: Rectangle):
    return rectangle.lenght * rectangle.width


# Now mocks with some methods
# I'm purposely not providing you the class representing a container.


def jsonify_docker_container(container):
    return {
        "image": container.image.tags,
        "name": container.name,
        "cpu": container.get_cpu_usage(),
        "ram": container.get_ram_usage(),
    }
