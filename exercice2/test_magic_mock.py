#!/usr/bin/env python3
from pytest import fixture
from unittest.mock import Mock, MagicMock

# Testing the __str__ magic method


@fixture
def mock():
    # will fail when calling mock[<index>]
    mock = Mock()
    mock.__str__.return_value = "tartiflette"
    return mock


@fixture
def magic_mock():
    # will work when calling Mock[<index>]
    mock = MagicMock()
    mock.__str__.return_value = "tartiflette"
    return mock


def test_mock(mock):
    assert str(mock) == "tartiflette"


def test_magic_mock(magic_mock):
    assert str(magic_mock) == "tartiflette"


# Testing the __getitem__ magic method ( dict["myobj"] )


@fixture
def mock_dict():
    # will fail when calling mock_dict[<index>]
    mock = Mock()
    mock.__getitem__.return_value = "tartiflette"
    return mock


@fixture
def magic_mock_dict():
    # will work when calling Mock_Dict[<index>]
    mock = MagicMock()
    mock.__getitem__.return_value = "tartiflette"
    return mock


def test_mock_dict(mock_dict):
    assert mock_dict["my_mocked_value"] == "tartiflette"


def test_magic_mock_dict(magic_mock_dict):
    assert magic_mock_dict["my_mocked_value"] == "tartiflette"
