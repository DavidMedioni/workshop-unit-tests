#!/usr/bin/env python3
import pytest
from pytest import fixture
from unittest.mock import Mock
from simple_mock import Rectangle, compute_rectangle_surface, jsonify_docker_container


@fixture
def mock_rectangle():
    mock = Mock()
    mock.lenght = 2
    mock.width = 5
    return mock


def test_compute_rectangle_surface(mock_rectangle):
    assert compute_rectangle_surface(mock_rectangle) == 10


@fixture
def image():
    return Mock(tags="mongo:4.0.10")


@fixture
def container(image):
    mock = Mock()
    mock.image = image
    mock.name = "my mongo container"
    mock.get_cpu_usage.return_value = "100%"
    mock.get_ram_usage.return_value = "5GB"
    return mock


def test_jsonify_docker_container(container):
    assert jsonify_docker_container(container) == {
        "image": "mongo:4.0.10",
        "name": "my mongo container",
        "cpu": "100%",
        "ram": "5GB",
    }
