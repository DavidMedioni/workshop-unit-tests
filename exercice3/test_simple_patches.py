#!/usr/bin/env python3
from pytest import fixture
from unittest.mock import patch, Mock
from simple_patches import CryptoTrading


@fixture
def bare_crypto_trading():
    """
    Return an instance of CryptoTrading without it's init method evaluated.
    """
    with patch.object(CryptoTrading, "__init__", return_value=None):
        return CryptoTrading()


@fixture
def coinbase_request():
    """Output from the api as per it's official documentation"""
    data = {"data": {"base": "BTC", "currency": "EUR", "amount": "10.00"}}

    mock_request = Mock(**{"json.return_value": data})
    mock_request.name = "coinbase_request"
    return mock_request


@fixture
def request_url():
    return "https://tartiflette.com"


def test_generate_request_url(bare_crypto_trading, coinbase_request):
    ct = bare_crypto_trading
    ct.base_url = "http://toto"
    ct.currency = "tartiflette"
    assert ct._generate_request_url() == "http://toto?currency=tartiflette"


@patch("simple_patches.CryptoTrading._generate_request_url", return_value="http://toto")
def test_get_bitcoin_price(url, coinbase_request, bare_crypto_trading):
    ct = bare_crypto_trading
    with patch("simple_patches.get", return_value=coinbase_request):
        price = ct.get_bitcoin_price()
    assert price == "10.00"
