#!/usr/bin/env python3
from requests import get

# doc : https://developers.coinbase.com/docs/wallet/guides/price-data
# url = "https://api.coinbase.com/v2/prices/spot"
# currency = "EUR"


class CryptoTrading:
    def __init__(self, base_url: str, currency: str):
        self.base_url = base_url
        self.currency = currency

    def _generate_request_url(self):
        return f"{self.base_url}?currency={self.currency}"

    def get_bitcoin_price(self):
        url = self._generate_request_url()
        data = get(url=url).json()
        return data["data"]["amount"]


def get_bitcoin_price_in_EUR():
    # Makes a web request, it's a 3rd dependency
    request = get(url="https://blockchain.info/ticker")
    # Get json content from the payload
    response = request.json()
    # Parse the JSON
    eur_latest_price = response["EUR"]["last"]
    return eur_latest_price
